from setuptools import setup, find_packages # Always prefer setuptools over distutils

setup(
    name='dst',
    version='0.1.0',
    description='Do Some Things',
    author='Colin Vallance',
    author_email='crvallance@keybase.io',
    url='https://bitbucket.org/crvallance/dst',
    packages=['do-some-things'],
    install_requires=['paramiko'],
    package_data={'do-some-things': ['input-examples/*.*']},
    #data_files=[('input-examples', ['cmd-list.txt', 'cred.csv'])],
    scripts=['do-some-things/dst.py']
)
