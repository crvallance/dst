#!/usr/bin/env python
import paramiko
import csv
import sys, getopt, optparse
import os
import re
import time
#from Tkinter import *
#from tkFileDialog import askopenfilename
from multiprocessing.pool import ThreadPool as Pool


def sshconn(credline, commands, timeout, mypath):
    addy = credline[0]
    user = credline[1]
    passw = credline[2]
    enable = credline[3]
    outputs = []
    if not timeout:
        mytimeout = 5
    else:
        mytimeout = timeout
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #Tries to make a ssh connection with paramiko. 
    #Logs errors out to a file and prints to screen
    try:
        client.connect(addy, username=user, password=passw, timeout=mytimeout)
    except:
        message = '[!] Could not Connect to: %s@%s\nUnexpected error: %s %s\n'\
             % (user, addy, str(sys.exc_info()[1]), str(sys.exc_info()[0]))
        print message
        errout(message, mypath, timestring)
        pass
    #setup shell and pass raw commands
    chan = client.invoke_shell()
    chan.settimeout(15.0)
    outputs += [slurp(chan)]
    if enable:
        chan.send('enable\n')
        chan.send(enable+'\n')
        time.sleep(1)
        outputs += [slurp(chan)]
    for cmd in commands:
        chan.send(cmd+'\n')
        time.sleep(1)
        outputs += [slurp(chan)]
    chan.close()
    return (outputs)

def wlcsshconn(credline, commands, timeout, mypath):
    addy = credline[0]
    user = credline[1]
    passw = credline[2]
    ctrlr = credline[4]
    outputs = []
    if not timeout:
        mytimeout = 5
    else:
        mytimeout = timeout
    client = paramiko.SSHClient()
    client.set_missing_host_key_policy(paramiko.AutoAddPolicy())
    #Tries to make a ssh connection with paramiko. 
    #Logs errors out to a file and prints to screen
    try:
        client.connect(addy, username=user, password=passw, timeout=mytimeout)
    except:
        message = '[!] Could not Connect to: %s@%s\nUnexpected error: %s %s\n'\
             % (user, addy, str(sys.exc_info()[1]), str(sys.exc_info()[0]))
        print message
        errout(message, mypath, timestring)
        pass
    #setup shell and pass raw commands
    chan = client.invoke_shell()
    chan.settimeout(1.0)
    outputs += [slurp(chan)]
    if ctrlr:
        chan.send(user+'\n')
        chan.send(passw+'\n')
        chan.send('config paging disable\n')
        time.sleep(1)
        outputs += [slurp(chan)]
    for cmd in commands:
        chan.send(cmd+'\n')
        time.sleep(1)
        outputs += [slurp(chan)]
    chan.close()
    return (outputs)

def slurp(chan):
    buff = ''
    while True:
        try:
            buff += chan.recv(256)
        except IOError:
            break
    lines = buff.split('\n')
    cleaned = '\n'.join(lines[0:len(lines)])
    return cleaned

def creds(credsfile):
    f = open(credsfile,'rb')
    reader = csv.reader(f)
    args = []
    for row in reader:   
        args += [row]
    f.close()
    validip = re.compile("^\d{1,3}\.\d{1,3}\.\d{1,3}\.\d{1,3}$")
    # This checks to see if there is a header
    # If the first value is an IP, there's no header
    if re.match(validip,args[0][0]):
        startrow = 0
    else:
        startrow = 1
    return (args, startrow)

def cmds(cmdfile):
    f2 = open(cmdfile,'rb')
    iplines = f2.readlines()
    f2.close()
    cleancmd = []
    for eachline in iplines:
        cleancmd += [eachline.strip('\r\n')]
    return cleancmd

def filesetup():
    Tk().withdraw()
    print 'Please select the credentials CSV'
    garbage = raw_input("Press Enter to continue...")
    credsfile = askopenfilename()
    print 'Please select the commands text file'
    garbage = raw_input("Press Enter to continue...")
    cmdfile = askopenfilename()
    mypath = os.path.dirname(credsfile)
    return (credsfile, cmdfile, mypath)

def errout(message, mypath, timestring):
    os.chdir(mypath)
    f = open('Errors'+timestring+'.log','a')
    f.write(message)
    f.close()

def results(devoutput, mypath, timestring, hostinfo):
    os.chdir(mypath)
    f = open('Output_'+timestring+'.txt','a')
    f.write('\n\n~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~Output for %s~~~~\
~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~\n\n' % hostinfo)
    for item in devoutput:
        f.write(item)

    f.close()

def worker(host, devtype, mycommands):
    print 'Running commands at %s\n' % host[0] 
    ctrlr = str(devtype)
    if 'wlc' in ctrlr.strip().lower():
        try:
            devoutput = wlcsshconn(host, mycommands, \
                timeout, mypath)
            results(devoutput, mypath, timestring, host[0])
        except:
            # Passing here because error is output above and we want to 
            # continue with other hosts
            pass
    else:
        try:
            devoutput = sshconn(host, mycommands, timeout, \
                mypath)
            results(devoutput, mypath, timestring, host[0])
        except:
            # Passing here because error is output above and we want to 
            # continue with other hosts
            pass


if __name__ == "__main__":
    #paramiko.util.log_to_file('paramiko_connection.log')
    desc="""%prog is a way to run a set of commands
across multiple devices automagically to save some time."""
    parser = optparse.OptionParser(description=desc, version='%prog v0.1.0')
    parser.add_option('-c', '--creds', help='Credentials file (required)', 
        dest='man1', action='store_true')
    parser.add_option('-r', '--runcmds', help='Run commands file (required)', 
        dest='man2', action='store_true')
    parser.add_option('-p', '--parallel', dest='thread_num', 
        help='Parallel sessions [defaults to %default]', type='int', default=1)
    (opts, args) = parser.parse_args()
    mandatories = ['man1', 'man2']
    for m in mandatories:
        if not opts.__dict__[m]:
            print "mandatory option is missing\n"
            parser.print_help()
            exit(-1)
    mypath = os.getcwd()
    pool_size = opts.thread_num
    credsfile, cmdfile = args
    now = time.localtime()
    timestring = ('%s%s%s%s%s%s') % (now[1],now[2],now[0],now[3],now[4],now[5])
    timeout = 5
    '''if len(sys.argv) >= 2:
        credsfile = sys.argv[1]
        cmdfile = sys.argv[2]
        mypath = os.getcwd()
    else:
        credsfile, cmdfile, mypath = filesetup()

    if not sys.argv[3] is None or False or []:
        pool_size = 5        
    else:
        pool_size = sys.argv[3]
    pool_size = 2'''
    pool = Pool(pool_size)

    device, startrow = creds(credsfile)

    mycommands = cmds(cmdfile)

    #for credline in range(startrow,len(device)):
    for host in device[startrow:]:
        pool.apply_async(worker, (host, host[4], mycommands))

    pool.close()
    pool.join()