__version__ = '0.1.0'
__author__ = 'Colin Vallance <crvallance@keybase.io>'
__repository__ = 'https://bitbucket.org/crvallance/dst'

def get_version_info():
    return {
        'version': __version__,
        'author': __author__,
        'website': __website__,
        'repository': __repository__,
    }

import dst