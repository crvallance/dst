# What is it?
dst.py (Do Some Things) is meant as an automation tool for networking gear.  This script allows you to 
(hopefully) ssh to several devices using specified credentials and run certain commands


# How do I use it?
    Usage: dst.py [options]
    
    Options:
      --version             show programs version number and exit
      -h, --help            show this help message and exit
      -c, --creds           Credentials file (required)
      -r, --runcmds         Run commands file (required)
      -p THREAD_NUM, --parallel=THREAD_NUM         
                            Parallel sessions [defaults to 1]


# How do I install it?
Either clone the repo and use setup tools or use pip from the internet.  Details below.

## Requirements

* Python 2.6,2.7
* paramiko (setup should deal with this dependency)
* pyCrypto (paramiko should deal with this dependency)  Windows has some issues with this, try [voidspace](http://www.voidspace.org.uk/python/modules.shtml) for precompiled installable versions.
 

## Installation
* Install using [pip](http://pip.readthedocs.org/en/latest/installing.html) directly from the internet (easier for the uninitiated)
```
#!bash
pip install https://bitbucket.org/crvallance/dst/get/master.tar.gz
```
***OR***

* Clone the repo and use setup tools
```
#!bash
git clone https://crvallance@bitbucket.org/crvallance/dst.git
python ./setup.py install
```

# Example Command File
This is just a flat file with all the commands you want to run on each device.  Note: in v0.1.0 you must explicitly
add in the appropriate command to disable paging.  Script will fail without it.
```
#!text
term leng 0
show cdp neigh
```

# Example Credential CSV File
This is a CSV with a (relatively) descriptive header.  This file contains the IP and credentials for all the devices you will run your command file commands on.  Be sure if you use the header it's commented out.
```
#!text
#IP,Login,Pass,Enable,wlc-or-switch
10.10.10.5,admin,funky,monkey!,switch
```


# ChangeLog

* 0.1.0
	* Initial Release
	* Just get this this propped up!
  * Actual example files are in site-packages\do-some-things\input-examples\

# Acks

* Thanks to Steve as always for the python help and examples.  This readme framework shamelessly ripped from dofler (https://github.com/SteveMcGrath/DoFler)